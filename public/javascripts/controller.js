var app = angular.module('check', []);
app.controller("AppCtrl",function ($scope,$http) {
    $('#abc').hide();
    $http.get('https://pixabay.com/api/?key=6314885-82c358fe0aa1fa3e20f8ee42a&q=nature&image_type=photo&pretty=true').then(function (response) {
        $scope.apiData=response.data.hits;
    }).catch(function (error) {
        console.log(error)
    });

    $scope.imageSelected=function(id) {
        $('#movies').hide();
        for(var i=0;i< $scope.apiData.length;i++){
            if(id==$scope.apiData[i].id){
                $('#abc').show();
                $scope.previewURL= $scope.apiData[i].previewURL;
                $scope.tags= $scope.apiData[i].tags;
            }
        }
    };
    $scope.showDetails=function () {
        $('#abc').hide();
        $('#movies').show();
    }
});

